/**
 * The class is destined for the homework assignment v 1.1 - The skeleton
 * attributed to jpoial. Src https://jpoial@bitbucket.org/i231/praktikum1.git
 */

public class Balls {

   /**
    * Only two values: 1. -RED, 2. green. NB! Sorting ordinarity is kept elsewhere.
    */
   enum Color {green, red}
   // Well, no semicolon: https://stackoverflow.com/questions/38172369/end-of-list-of-enums-in-java

   // References:
     // That story starts here: https://bitbucket.org/furunkel027/praktikum1/
     // In 2015 I had freshly passed the Java course and had time to Google.
     // Background theory here:
     //    Born in Moscow - http://en.wikipedia.org/wiki/Quicksort
     // Some good pseudo-code was available here:
     //    http://www.myassignmenthelp.net/quicksort-assignment-help.php
     // A doublegood bellyfeel sorting video:
     //      https://www.youtube.com/watch?v=vxENKlcs2Tw

   /**
    *  MAIN is mostly needed for debugging and array insertion
    * @param param String[] is passed from the junit BallsTest
    */
   public static void main (String[] param) {
      // for debugging
      System.out.println("Me want an array of test balls to sort!");
   }


    /**
     * A signature method (mandatory wrapper) to satisfy the automated tests
     * @param balls of Color[] pointing to the array to sort
     */
    public static void reorder (Color[] balls) {
        // Special cases
        if (balls.length < 2) // difficult to sort a Globe ;)
            return;
        // init the boundaries and launch the game
        int left = 0;
        int right = (balls.length - 1); // yeah, base 0
        quickSort(balls, left, right);
    }


    /**
     * This is where the actual magic will happen
     * @param balls The array we deal with
     * @param lo a lower boundary
     * @param hi a higher boundary
     * @return nothing. A generic quickSort would still need the int median returned
     */
    public static void quickSort(Color[] balls, int lo, int hi) {
        int size = (hi - lo);
        if (size < 2) // too few balls
            return;
        // Reds must go to the left side
        int splitPoint = lo;
        for (int i = lo; (i < (hi + 1)); i++) {
            if (isRed(balls, i)) {
                swapAB(balls, i, splitPoint); // any sense to check (i==splitpoint)  ;)
                splitPoint++;
            }
        }
        return;
    }

    /**
     *
     * @param aBall The array we deal with
     * @param i the element of the array we test
     * @return either true=red or false.
     */
    public static boolean isRed(Balls.Color[] aBall, int i) {

        Color pivotValue = Balls.Color.green; // For a two-value enum
        boolean ballIsRed = !aBall[i].equals(pivotValue);
        return ballIsRed;
    }


   /**
    * This is the swapping instrument.
    * @param balls - the name of the array to work with
    * @param a the position of the ball A
    * @param b the position of the ball B
    */
   public static void swapAB(Balls.Color balls[], int a, int b) {
       // NB! the algo is actually transparent to the (a==b) case.
      Color tempStorage = balls[a];
      balls[a] = balls[b];
      balls[b] = tempStorage;
      return;
   }

}

